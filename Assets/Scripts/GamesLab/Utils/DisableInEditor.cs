using UnityEngine;

namespace GamesLab.Utils
{
    public class DisableInEditor : MonoBehaviour
    {
        private void Awake()
        {
            if (Application.isEditor) {
                gameObject.SetActive(false);
            }
        }
    }
}