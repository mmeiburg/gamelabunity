namespace GamesLab.Utils
{
    using UnityEngine;

    public class PreventObjectToGoOffscreen : MonoBehaviour
    {
        private Camera cam;
        public Vector2 offset = new Vector2(-0.1f, 1.1f);

        private void OnEnable()
        {
            cam = Camera.main;
        }

        private void LateUpdate()
        {
            Vector3 pos = cam.WorldToViewportPoint(transform.position);

            bool onScreen = pos.z > 0 && pos.x > 0 && pos.x < 1 && pos.y > 0 && pos.y < 1;

            if (onScreen) {
                return;
            }

            pos.x = Clamp(pos.x);
            pos.y = Clamp(pos.y);
            transform.position = cam.ViewportToWorldPoint(pos);
        }

        private float Clamp(float value)
        {
            return Mathf.Clamp(value, offset.x, offset.y);
        }
    }
}