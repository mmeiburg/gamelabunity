using SmartData.SmartInt;
using UnityEngine;
using UnityEngine.UI;

namespace GamesLab.Utils.UI
{
    public class SetFillAmountInt : MonoBehaviour
    {
        public Image image;
        public IntReader maxAmount;

        public void SetInt(int amount)
        {
            image.fillAmount = amount / (float)maxAmount.value;
        }
    }
}