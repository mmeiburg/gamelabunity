namespace GamesLab.Utils
{
    using UnityEngine;

    public class UnityAccelerationTextSetter : TextSetter<Vector3>
    {
        public override Vector3 GetValue()
        {
            return Input.acceleration;
        }
    }
}