namespace GamesLab.Utils
{
    using SmartData.SmartInt.Data;
    using UnityEngine;

    public class IntVarTextSetter : TextSetter<int>
    {
        [SerializeField]
        private IntVar intVar;
        
        public override int GetValue()
        {
            return intVar.value;
        }
    }
}