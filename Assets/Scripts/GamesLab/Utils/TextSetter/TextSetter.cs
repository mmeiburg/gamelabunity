namespace GamesLab.Utils
{
    using TMPro;
    using UnityEngine;

    public abstract class TextSetter<T> : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI textMeshPro;
        
        private void Update()
        {
            if (textMeshPro == null) {
                return;
            }
            
            textMeshPro.text = GetValue().ToString();
        }

        public abstract T GetValue();
    }
}