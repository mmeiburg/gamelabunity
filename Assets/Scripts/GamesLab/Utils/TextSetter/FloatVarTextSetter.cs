namespace GamesLab.Utils
{
    using SmartData.SmartFloat.Data;
    using UnityEngine;

    public class FloatVarTextSetter : TextSetter<float>
    {
        [SerializeField]
        private FloatVar floatVar;
        
        public override float GetValue()
        {
            return floatVar.value;
        }
    }
}