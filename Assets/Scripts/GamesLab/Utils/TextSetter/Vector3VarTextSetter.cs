namespace GamesLab.Utils
{
    using SmartData.SmartVector3.Data;
    using UnityEngine;

    public class Vector3VarTextSetter : TextSetter<Vector3>
    {
        [SerializeField]
        private Vector3Var vector;

        public override Vector3 GetValue()
        {
            return vector.value;
        }
    }
}