namespace GamesLab.Utils
{
    using System;
    using SmartData.SmartEvent.Data;
    using UnityEngine.Events;

    [Serializable]
    public class EventInvoker
    {
        public EventVar eventVar;
        public UnityEvent unityEvent;
        public bool useEventVar = true;

        public void Invoke()
        {
            if (useEventVar) {
                if (eventVar != null) {
                    eventVar.Dispatch();
                }
            } else {
                unityEvent?.Invoke();
            }
        }
    }
}