namespace GamesLab.Demo
{
    using UnityEngine;

    [CreateAssetMenu]
    public class LevelBounds : ScriptableObject
    {
        public int width = 11;
        public int height = 22;
    }
}