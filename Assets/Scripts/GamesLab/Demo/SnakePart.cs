namespace GamesLab.Demo
{
    using UnityEngine;

    public abstract class SnakePart : MonoBehaviour
    {
        protected Snake snake;

        protected virtual void Awake()
        {
            snake = GetComponent<Snake>();
        }
    }
}