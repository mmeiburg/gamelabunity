namespace GamesLab.Demo
{
    using Actions;
    using DG.Tweening;
    using SmartData.SmartEvent.Data;
    using UnityEngine;

    [RequireComponent(typeof(Snake))]
    public class SnakeMouth : SnakePart
    {
        public bool Ate { get; set; }
        [SerializeField]
        private ScreenShake screenShake;
        [SerializeField]
        private EventVar onAteMyself;
        [SerializeField]
        private EventVar onAteFood;

        private void Feed()
        {
            snake.score.value++;
            Ate = true;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("SnakeTail")) {
                StartCoroutine(screenShake.StartShaking(AteMyself));
                return;
            }
            
            ProceedFood(other);
        }

        private void AteMyself()
        {
            onAteMyself?.Dispatch();
        }

        private void ProceedFood(Collider2D other)
        {
            onAteFood?.Dispatch();
            
            Food food = other.GetComponentInParent<Food>();
            
            if (!food) {
                return;
            }

            Feed();
            food.Ate();

            snake.model.transform
                .DOPunchScale(Vector3.one, 0.2f)
                .OnComplete(() =>
                {
                    snake.model.transform.localScale = Vector3.one;
                });
        }
    }
}