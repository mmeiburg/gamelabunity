namespace GamesLab.Demo
{
    using SmartData.SmartInt.Data;
    using UnityEngine;

    public class HighscoreSaver : MonoBehaviour
    {
        [SerializeField]
        private IntVar score;
        [SerializeField]
        private IntVar highscore;
        
        public void Save()
        {
            if (score.value > highscore.value) {
                PlayerPrefs.SetInt("Highscore", score.value);
                highscore.value = score.value;
            }
        }
    }
}