namespace GamesLab.Demo
{
    using SmartData.SmartEvent.Data;
    using UnityEngine;

    public class LevelBoundsController : MonoBehaviour
    {
        public EventVar onGameOver;
        
        [SerializeField]
        private LevelBounds levelBounds;
        [SerializeField]
        private Snake snake;

        private void Update()
        {
            if (IsOutOfBounds(snake.transform.position)) {
                GameOver();
            }
        }

        private bool IsOutOfBounds(Vector2 position)
        {
            int x = (int) position.x;
            int y = (int) position.y;

            return x == levelBounds.width ||
                   x == -levelBounds.width ||
                   y == levelBounds.height ||
                   y == -levelBounds.height;
        }

        private void GameOver()
        {
            if (onGameOver == null) {
                return;
            }
            
            onGameOver.Dispatch();
        }
    }
}