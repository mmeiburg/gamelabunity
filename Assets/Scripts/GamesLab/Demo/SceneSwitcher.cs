namespace GamesLab.Demo
{
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public class SceneSwitcher : MonoBehaviour
    {
        [SerializeField]
        private string sceneName; 
        
        public void Switch()
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}