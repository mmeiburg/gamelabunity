namespace GamesLab.Actions
{
    using DG.Tweening;
    using UnityEngine;

    public class OverlayAction : MonoBehaviour
    {
        [SerializeField]
        private SpriteRenderer spriteRenderer;

        [SerializeField]
        private Color lightOffColor;
        [SerializeField]
        private Color lightOnColor;

        [SerializeField]
        private float lerp = 0.3f;
        
        private void Awake()
        {
            Color c = spriteRenderer.color;
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void StartSleep()
        {
            spriteRenderer.DOColor(lightOffColor, lerp);
        }

        public void EndSleep()
        {
            spriteRenderer.DOColor(lightOnColor, lerp);
        }
    }
}