using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace GamesLab.Actions
{
    public class ActionOnStart : MonoBehaviour
    {
        public UnityEvent onStart;

        private void Start()
        {
            onStart?.Invoke();
        }
    }
}