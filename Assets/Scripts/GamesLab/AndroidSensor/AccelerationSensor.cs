namespace GamesLab.AndroidSensor
{
    using SmartData.SmartVector3.Data;
    using UnityEngine;

    public class AccelerationSensor : SensorReader
    {
        [SerializeField]
        private Vector3Var value;
        
        private const float Alpha = 10f;
        
        protected override void Execute()
        {
            if (value == null) {
                return;
            }
            
            float[] values = Values;
            
            value.value = 
                new Vector3(LowPass(values[0]), LowPass(values[1]), LowPass(values[2])) * -1f;
            
            // Could also archive with build in unity methods
            // x.value = Input.acceleration.x;
            // y.value = Input.acceleration.y;
            // z.value = Input.acceleration.z;
        }

        protected override Sensor GetSensor()
        {
            return Sensor.accelerometer;
        }

        private float LowPass(float value)
        {
            return Mathf.Round((value / Alpha) * 100f) / 100f;
        }
    }
}