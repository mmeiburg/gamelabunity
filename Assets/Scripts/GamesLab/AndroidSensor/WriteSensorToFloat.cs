using SmartData.SmartFloat.Data;
using UnityEngine;

namespace GamesLab.AndroidSensor
{
    public class WriteSensorToFloat : MonoBehaviour
    {
        [Header("Sensor")]
        public Sensor sensor;
        public int sensorIndex;
        
        [Header("Variable")]
        public FloatVar floatVar;
        
        private void Start()
        {
            UnitySensorPlugin.Instance.StartListenting(sensor);
        }

        private void Update()
        {
            floatVar.value = UnitySensorPlugin.Instance.GetSensorValue(sensor)[sensorIndex];
        }
    }
}