﻿using UnityEngine;

namespace GamesLab.AndroidSensor
{
    public enum Sensor
    {
        accelerometer,
        ambienttemperature,
        gamerotationvector,
        geomagneticrotationvector,
        gravity,
        gyroscope,
        gyroscopeuncalibrated,
        heartrate,
        light,
        linearacceleration,
        magneticfield,
        magneticfielduncalibrated,
        pressure,
        proximity,
        relativehumidity,
        rotationvector,
        significantmotion,
        stepcounter,
        stepdetector,
    }

    public class UnitySensorPlugin : MonoBehaviour
    {
        public static UnitySensorPlugin Instance { get; private set; }

        private const string SensorValues = "getSensorValues";
        private const string StartListening = "startSensorListening";
        
        /* Reference to java instance*/
        private AndroidJavaObject plugin;

        private void Awake()
        {
            if (Instance == null) {
                Instance = this;
                
                #if UNITY_ANDROID
                    InitializePlugin();
                #endif
                
                DontDestroyOnLoad(this);
            } else if(Instance != this) {
                Destroy(this);
            }
        }

        public void StartListenting(Sensor sensor)
        {
            #if UNITY_ANDROID
                plugin?.Call(StartListening, sensor.ToString().ToLower());
            #endif
        }

        public float[] GetSensorValue(Sensor sensor)
        {
            if (plugin == null) {
                return new float[1];
            }
            
            #if UNITY_ANDROID
                return plugin.Call<float[]>(SensorValues, sensor.ToString().ToLower());
            #endif

            return null;
        }
        
        private void OnApplicationQuit ()
        {
            #if UNITY_ANDROID
                TerminatePlugin();
            #endif
        }
        
        private void InitializePlugin()
        {
            using (AndroidJavaClass pluginClass =
                new AndroidJavaClass("de.marcusmeiburg.plugin.UnitySensorPlugin")) {
                
                plugin = pluginClass
                    .CallStatic<AndroidJavaObject>("getInstance");
            }
        }

        private void TerminatePlugin()
        {
            if (plugin == null) {
                return;
            }

            plugin.Call("terminate");
            plugin = null;
        }
    }
}