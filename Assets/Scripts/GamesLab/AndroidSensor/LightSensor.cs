namespace GamesLab.AndroidSensor
{
    using SmartData.SmartFloat.Data;
    using UnityEngine;
    
    [DisallowMultipleComponent]
    public class LightSensor : SensorReader
    {      
        [SerializeField]
        private FloatVar value;
        
        protected override void Execute()
        {
            if (value == null) {
                return;
            }
            
            value.value = Values[0];
        }
        
        protected override Sensor GetSensor()
        {
            return Sensor.light;
        }
    }
}