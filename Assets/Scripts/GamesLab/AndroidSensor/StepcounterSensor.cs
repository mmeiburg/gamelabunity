namespace GamesLab.AndroidSensor
{
    using SmartData.SmartInt.Data;
    using UnityEngine;

    [DisallowMultipleComponent]
    public class StepcounterSensor : SensorReader
    {
        [SerializeField]
        private IntVar value;
        
        protected override void Execute()
        {
            if (value == null) {
                return;
            }
            
            value.value = (int)Values[0];
        }
        
        protected override Sensor GetSensor()
        {
            return Sensor.stepcounter;
        }
    }
}