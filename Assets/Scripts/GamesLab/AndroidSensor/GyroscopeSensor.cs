namespace GamesLab.AndroidSensor
{
    using SmartData.SmartVector3.Data;
    using UnityEngine;

    [DisallowMultipleComponent]
    public class GyroscopeSensor : SensorReader
    {
        [SerializeField]
        private Vector3Var value;

        protected override void Execute()
        {
            if (value == null) {
                return;
            }
            
            float[] values = Values;

            value.value =
                new Vector3(values[0], values[1], values[2]);
        }
        
        protected override Sensor GetSensor()
        {
            return Sensor.gyroscopeuncalibrated;
        }
    }
}