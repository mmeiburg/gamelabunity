﻿using UnityEngine;

namespace GamesLab.AndroidSensor
{
    public abstract class SensorReader : MonoBehaviour
    {
#region Variables
        [Tooltip("Updates every x frames")]
        [Range(1,10)]
        [SerializeField]
        private int updateInterval = 3;
#endregion

#region Properties
        protected float[] Values => UnitySensorPlugin.Instance.GetSensorValue(GetSensor());
#endregion

        private void Start()
        {
            UnitySensorPlugin.Instance.StartListenting(GetSensor());
        }
        
        private void Update()
        {
            if (Time.frameCount % updateInterval == 0) {
                Execute();
            }
        }

        protected abstract Sensor GetSensor();
        protected virtual void Execute() {}
    }
}