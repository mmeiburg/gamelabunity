namespace GamesLab.AndroidSensor
{
    using SmartData.SmartEvent.Data;
    using SmartData.SmartVector3.Data;
    using UnityEngine;

    public class ShakeDetector : MonoBehaviour
    {
        [SerializeField]
        private Vector3Var acceleration;
        [SerializeField]
        private EventVar onShakeEvent;

        private const float AccelerometerUpdateInterval = 1.0f / 60.0f;
        private const float LowPassKernelWidthInSeconds = 1.0f;

        private float shakeDetectionThreshold = 2.0f;
        private float lowPassFilterFactor;
        private Vector3 lowPassValue;

        private void Start()
        {
            lowPassFilterFactor = AccelerometerUpdateInterval / LowPassKernelWidthInSeconds;
            shakeDetectionThreshold *= shakeDetectionThreshold;
            lowPassValue = Input.acceleration;
        }

        private void Update()
        {
            lowPassValue = Vector3.Lerp(lowPassValue, acceleration.value, lowPassFilterFactor);
            Vector3 deltaAcceleration = acceleration.value - lowPassValue;

            if (deltaAcceleration.sqrMagnitude >= shakeDetectionThreshold) {
                onShakeEvent?.Dispatch();
            }
        }
    }
}