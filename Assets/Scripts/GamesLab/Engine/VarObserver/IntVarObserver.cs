using SmartData.SmartInt.Data;
using UnityEngine;

namespace GamesLab.Engine.VarObserver
{
    using Utils;

    public class IntVarObserver : MonoBehaviour
    {
        public VariableAndEvents variableAndEvents;
        public MinVariableAndEvents minVariableAndEvents;
        public MaxVariableAndEvents maxVariableAndEvents;

        private int lastValue;
        
        private void OnEnable()
        {
            if (variableAndEvents.variable != null) {
                variableAndEvents.variable.relay.AddListener(OnValueChanged);
            }
        }

        private void OnDisable()
        {
            if (variableAndEvents.variable != null) {
                variableAndEvents.variable.relay.RemoveListener(OnValueChanged);
            }
        }
        
        private void OnValueChanged(int newValue)
        {
            if (newValue > lastValue) {
                ValueIncreased(newValue);
                lastValue = newValue;
            } else if (newValue < lastValue) {
                ValueDecreased(newValue);
                lastValue = newValue;
            }
        }

        private void ValueIncreased(int newValue)
        {
            if (ValueAtMax(lastValue)) {
                return;
            }
            
            variableAndEvents.onValueIncreased?.Invoke();
            
            if (ValueAtMin(lastValue)) {
                minVariableAndEvents.onNoLongerAtMinValue?.Invoke();
            }
            
            if (ValueAtMax(newValue)) {
                maxVariableAndEvents.onMaxValueReached?.Invoke();
            }
        }
        
        private void ValueDecreased(int newValue)
        {
            if (ValueAtMin(lastValue)) {
                return;
            }
            
            variableAndEvents.onValueDecreased?.Invoke();
            
            if (ValueAtMax(lastValue)) {
                maxVariableAndEvents.onNoLongerAtMaxValue?.Invoke();
            }
            
            if (ValueAtMin(newValue)) {
                minVariableAndEvents.onMinValueReached?.Invoke();
            }
        }

        private bool ValueAtMax(int value)
        {
            return value >= maxVariableAndEvents.maxValue.value;
        }

        private bool ValueAtMin(int value)
        {
            return value <= minVariableAndEvents.minValue.value;
        }
              
        [System.Serializable]
        public class VariableAndEvents
        {
            public IntVar variable;
            public EventInvoker onValueDecreased;
            public EventInvoker onValueIncreased;
        }
        
        [System.Serializable]
        public class MinVariableAndEvents
        {
            public IntVar minValue;
            public EventInvoker onMinValueReached;
            public EventInvoker onNoLongerAtMinValue;
        }
        
        [System.Serializable]
        public class MaxVariableAndEvents
        {
            public IntVar maxValue;
            public EventInvoker onMaxValueReached;
            public EventInvoker onNoLongerAtMaxValue;
        }
    }
}