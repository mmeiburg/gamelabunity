﻿namespace GamesLab.Engine.VarComparator.Editor
{
    using UnityEditor;

    [CustomEditor(typeof(FloatVarComparator)), CanEditMultipleObjects]
    public class FloatVarComparatorDrawer : AbstractVarComparatorDrawer { }
}