﻿namespace GamesLab.Engine.VarComparator.Editor
{
    using UnityEditor;

    [CustomEditor(typeof(IntVarComparator)), CanEditMultipleObjects]
    public class IntVarComparatorDrawer : AbstractVarComparatorDrawer { }
}