﻿namespace GamesLab.Engine.VarComparator.Editor
{
    using UnityEditor;
    using UnityEngine;

    [CanEditMultipleObjects]
    public class AbstractVarComparatorDrawer : Editor
    {
        private SerializedProperty constant;
        private SerializedProperty onEvent;
        private SerializedProperty option;
        private SerializedProperty useConst;
        private SerializedProperty var1;
        private SerializedProperty var2;

        private void OnEnable()
        {
            var1 = serializedObject.FindProperty("var1");
            option = serializedObject.FindProperty("option");
            var2 = serializedObject.FindProperty("var2");
            onEvent = serializedObject.FindProperty("onEvent");
            constant = serializedObject.FindProperty("constant");
            useConst = serializedObject.FindProperty("useConst");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            GUILayout.BeginVertical();

            GUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(var1, GUIContent.none, true, GUILayout.MinWidth(100));
            EditorGUILayout.PropertyField(option, GUIContent.none, true, GUILayout.MinWidth(100));

            GUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(useConst, GUIContent.none, true, GUILayout.MaxWidth(20));

            if (useConst.boolValue) {
                EditorGUILayout.PropertyField(constant, GUIContent.none, true, GUILayout.MinWidth(100));
            } else {
                EditorGUILayout.PropertyField(var2, GUIContent.none, true, GUILayout.MinWidth(100));
            }

            GUILayout.EndHorizontal();

            GUILayout.EndHorizontal();

            GUILayout.Label("Trigger Event", GUILayout.Width(80));
            EditorGUILayout.PropertyField(onEvent, GUIContent.none, true, GUILayout.MinWidth(100));

            GUILayout.EndVertical();

            serializedObject.ApplyModifiedProperties();
        }
    }
}