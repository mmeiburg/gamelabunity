﻿namespace GamesLab.Engine.VarComparator
{
    using SmartData.SmartInt.Data;

    public class IntVarComparator : AbstractVarComparator<IntVar, int> { }
}