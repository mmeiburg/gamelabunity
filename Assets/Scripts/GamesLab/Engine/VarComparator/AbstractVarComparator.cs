﻿namespace GamesLab.Engine.VarComparator
{
    using System;
    using SmartData.Abstract;
    using UnityEngine;
    using UnityEngine.Events;

    public enum CompareOptions
    {
        Disabled,
        Equal,
        Greater,
        GreaterEqual,
        Less,
        LessEqual
    }

    public abstract class AbstractVarComparator<TVarType, TType> : MonoBehaviour
        where TVarType : SmartVar<TType>
        where TType : IComparable<TType>
    {
        public TType constant;

        [SerializeField] public UnityEvent onEvent;

        public CompareOptions option;

        [Tooltip("Use constant")] public bool useConst;

        public TVarType var1;
        public TVarType var2;

        private void OnEnable()
        {
            if (var1 != null) {
                var1.relay.AddListener(OnValueChanged);
            }

            if (var2 != null) {
                var2.relay.AddListener(OnValueChanged);
            }
        }

        private void OnDisable()
        {
            if (var1 != null) {
                var1.relay.RemoveListener(OnValueChanged);
            }

            if (var2 != null) {
                var2.relay.RemoveListener(OnValueChanged);
            }
        }

        private void OnValueChanged(TType type)
        {
            if (option == CompareOptions.Disabled) {
                return;
            }

            if (var1 == null) {
                Debug.Log("Var1 should be set");
                return;
            }

            if (var2 == null && !useConst) {
                Debug.Log("Var2 should be set");
                return;
            }

            int compare = var1.value.CompareTo(useConst ? constant : var2.value);

            if (option == CompareOptions.GreaterEqual && compare >= 0 ||
                option == CompareOptions.Equal && compare == 0 ||
                option == CompareOptions.Greater && compare > 0 ||
                option == CompareOptions.Less && compare < 0 ||
                option == CompareOptions.LessEqual && compare <= 0) {
                onEvent?.Invoke();
            }
        }
    }
}