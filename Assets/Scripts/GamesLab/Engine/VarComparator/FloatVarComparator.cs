﻿namespace GamesLab.Engine.VarComparator
{
    using SmartData.SmartFloat.Data;

    public class FloatVarComparator : AbstractVarComparator<FloatVar, float> { }
}