namespace GamesLab.Engine.VarCounter
{
    using SmartData.SmartInt.Data;
    using UnityEngine;

    public class IntVarArithmetic : MonoBehaviour
    {
        public IntVar variable;

        public void Add(int amount)
        {
            variable.value += amount;
        }

        public void Remove(int amount)
        {
            variable.value -= amount;
        }
    }
}