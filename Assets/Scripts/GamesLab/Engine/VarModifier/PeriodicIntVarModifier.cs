﻿namespace GamesLab.Engine.VarModifier
{
    using JetBrains.Annotations;
    using MinMaxVar;
    using UnityEngine;
    using Time = UnityEngine.Time;
    
    public class PeriodicIntVarModifier : MonoBehaviour
    {
        public MinMaxIntVar var;
        public PeriodicIntVarModifierSettings periodicModifyingSettings;

        private bool canModify;
        private float modifyTime = float.MaxValue;
        
        private void Update()
        {
            if (!canModify || modifyTime > Time.time) {
                return;
            }

            var.Value += periodicModifyingSettings.modifyAmount;
            RefreshModifyTime(modifyTime);
        }
        
        [UsedImplicitly]
        public void StartModifying()
        {
            if (canModify) {
                return;;
            }
            
            RefreshModifyTime(Time.time);
            canModify = true;
        }

        [UsedImplicitly]
        public void StopModifying()
        {
            if (!canModify) {
                return;
            }
            
            canModify = false;
            modifyTime = int.MaxValue;
        }
        
        public void RefreshModifyTime()
        {
            RefreshModifyTime(Time.time);
        }
        
        public void RefreshModifyTime(float startTime)
        {
            modifyTime = startTime + periodicModifyingSettings.interval;
        }
    }
}