namespace GamesLab.Engine.VarModifier
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "PeriodicIntVarModifierSettings", menuName = "Settings/PeriodicIntVarModifier")]
    public class PeriodicIntVarModifierSettings : ScriptableObject
    {
        public int modifyAmount;
        public int interval;
    }
}