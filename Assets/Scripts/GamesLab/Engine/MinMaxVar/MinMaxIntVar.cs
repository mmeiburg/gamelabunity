namespace GamesLab.Engine.MinMaxVar
{
    using SmartData.SmartInt.Data;
    using UnityEngine;

    [CreateAssetMenu(fileName = "MinMaxInt", menuName = "Data/Var/MinMaxInt")]
    public class MinMaxIntVar : ScriptableObject
    {
        public IntVar var;
        public IntVar varMin;
        public IntVar varMax;

        public int Value {
            get => var.value;
            set {
                if (value > varMax.value) {
                    var.value = varMax.value;
                    return;
                }

                if (value < varMin.value) {
                    var.value = varMin.value;
                    return;
                }

                var.value = value;
            }
        }

        public bool AtMax()
        {
            return var.value >= varMax.value;
        }

        public bool AtMin()
        {
            return var.value <= varMin.value;
        }

        public int minValue()
        {
            return varMin.value;
        }
        
        public int maxValue()
        {
            return varMax.value;
        }
    }
}